//Configuration minimale
const express = require('express');
const app = express();

const mongoose = require('mongoose');
const port = 4000;
let controller = require('./app/routes/taskRoutes')

//Configuration database
const uri = "mongodb://localhost/todolist_db"
mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => console.log(`[Mongo connecté !]`))
    .catch(err => {
        console.log(Error, err.message);
    })

//Route home
app.use(express.static(__dirname + '/public'));
app.set('view engine', "pug");
app.set("views", "./app/views");
app.use(express.urlencoded({extended: true}));
app.use(express.json());

app.get('/tasks', (req, res) => {
    res.render("tasks", { message: " Bienvenue sur ta to do list !" });
})

app.route('/tasks')
  .get(controller.getTasks)
  .get(controller.formTask)
  .post(controller.postTask)

app.route('/tasks/:id')
  .get(controller.getTask)
  .post(controller.updateTask)
app.route('/tasks/:id/edit')
  .get(controller.editTask)
app.route('/tasks/:id/delete')
  .get(controller.deleteTask)

app.listen(port, () => {
    console.log(`[ Le server est actif sur le port : ${port}]`);
})