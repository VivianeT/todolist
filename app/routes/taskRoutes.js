const mongoose = require('mongoose');
let Task = require('../models/task')


getTasks = (req,res) => {
  Task.find({}, (err, tasks) => {
    if (err) throw err;
    res.render("tasks",{tasks: tasks});
  })
}
formTask = (req, res) => {
  res.render("_form")
}

postTask = (req,res) => {
  let newTask = new Task(req.body);
  newTask.save((err, task) => {
    if (err) throw err;
    console.log(res);
    res.redirect("/tasks")
  })
}
getTask = (req, res) => {
    Task.findById(req.params.id, (err, task)=> {
    if (err) throw err;
    res.render("task",{task:task});
  });
}
deleteTask = (req,res) => {
    Task.remove({_id: req.params.id}, (err) => {
    res.redirect("/tasks")
  })
}
editTask = (req,res) => {
    Task.findById({_id: req.params.id}, (err,task)=> {
    if(err) throw err;
    res.render("_edit",{task:task})
  })
}
updateTask = (req,res) => {
    Task.findById({_id: req.params.id}, (err, task) => {
    if (err) throw err;
    Object.assign(task, req.body).save((err, utask) => {
      if (err) throw err;
      res.redirect("/tasks")
    })
  })
}

module.exports = { getTasks, postTask, getTask, deleteTask,editTask, updateTask, formTask}